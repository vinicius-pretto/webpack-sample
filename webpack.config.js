const { resolve } = require('path');

module.exports = {
  context: resolve('src'),
  entry: './index.js',
  output: {
    path: resolve('public'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader'
      }
    ]
  }
}